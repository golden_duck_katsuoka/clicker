﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum ResourcesType
{
    Force,
    One,
    Metal,
    Fuel,
    Gunpowder,
    Ether,
    ReaMetal,
}


public enum OneFacilityType
{
    Net,
    LargeNet,
    Vacuum,
    LittleDrone,
    Drone,
}

public enum MetalFacilityType
{
}

public enum FuelFacilityType
{
}

public enum GunpowderFacilityType
{
}

public enum EtherFacilityType
{
}

public enum ReaMetalFacilityType
{
}




namespace Master.Facility
{
    /// <summary>
    /// 施設マスター
    /// </summary>
    public class FacilityName
    {
        public int FacilityID;
        public int ResourcesType; // enum ResourcesType
        public int Rank;            // 表示上のランク
        public string Name;     // 名前
    }

    public class FacilityBase
    {    
        public int FacilityID { get; set; }
        public int Rank { get; set; }
        public string name { get; set; }
        public double IntervalSeconds { get; set; }// 増加間隔(s)
        public int BaseValue { get; set; }// 基本値
        public int LevelAddition { get; set; }//加算値
        public float LevelMagnitude { get; set; }// レベルごとの上昇率
    }

    public class FacilityNecessaryTable
    {
        public int FacilityID;      // 施設タイプ
        public int Rank { get; set; }
        public int MinLevel;        // 適用する最小レベル
        public int NeedResourcesType;   // 必要な資源タイプ
        public int NeedValueBase;           // 必要な値の基本値
        public float NeedAddingMagnitude;           // レベルごとに増加する必要数

    }


}