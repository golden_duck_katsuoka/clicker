﻿using Master.Event;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;

/// <summary>
/// クリックした時に増える資材のステータス
/// </summary>
public enum ClickFrontStateType
{
    One,
    Earth,
    Metal,
    Fuel,
    Gunpowder,
    Ether,
    ReaMetal,
    Will,
}

namespace ClickerData {

    [System.Serializable]
    public class UserData
    {
        public BigInteger Force; //{ set { _Force = value.ToString(); }        get { return BigInteger.Parse(_Force); } }
        public BigInteger One;//  { set { _Mud = value.ToString(); }          get { return BigInteger.Parse(_Mud); } }
        public BigInteger Metal;//     { set { _Metal = value.ToString(); }        get { return BigInteger.Parse(_Metal); } }
        public BigInteger Fuel;//  { set { _Fuel = value.ToString(); }         get { return BigInteger.Parse(_Fuel); } }
        public BigInteger Gunpowder;// { set { _Gunpowder = value.ToString(); }    get { return BigInteger.Parse(_Gunpowder); } }
        public BigInteger Ether;//  { set { _Ether = value.ToString(); }        get { return BigInteger.Parse(_Ether); } }
        public BigInteger ReaMetal;//  { set { _ReaMetal = value.ToString(); }     get { return BigInteger.Parse(_ReaMetal); } }
        public BigInteger Earth;//  { set { _ReaMetal = value.ToString(); }     get { return BigInteger.Parse(_ReaMetal); } }

        [SerializeField] private string _Force = "0";
        [SerializeField] private string _One = "0";
        [SerializeField] private string _Earth = "0";
        [SerializeField] private string _Metal = "0";
        [SerializeField] private string _Fuel = "0";
        [SerializeField] private string _Gunpowder = "0";
        [SerializeField] private string _Ether = "0";
        [SerializeField] private string _ReaMetal = "0";

        public Facilitys Facilitys = new Facilitys();
        
        public List<UserEventProgress>      UserEventProgress;
    }

[System.Serializable]
    public class Facilitys
    {
        public OneFacilityLevels One = new OneFacilityLevels();
        public EarthFacilityLevels Earth = new EarthFacilityLevels();
        public MetalFacilityLevels Metal = new MetalFacilityLevels();
        public FuelFacilityLevels Fuel = new FuelFacilityLevels();
        public GunpowderFacilityLevels Gunpowder = new GunpowderFacilityLevels();
        public EtherFacilityLevels Ether = new EtherFacilityLevels();
        public ReaMetalFacilityLevels ReaMetal = new ReaMetalFacilityLevels();
        public ForceFacilityLevels Force = new ForceFacilityLevels();
    }

[System.Serializable]
    public class OneFacilityLevels
    {
        public ulong Net = 1;
        public ulong LargeNet;
        public ulong Vacuum;
        public ulong LittleDrone;
        public ulong Drone;
    }


    [System.Serializable]
    public class EarthFacilityLevels
    {
        public ulong aa;
    }
    [System.Serializable]
    public class MetalFacilityLevels
    {
        public ulong aa;
    }
    [System.Serializable]
    public class FuelFacilityLevels
    {
        public ulong aa;
    }
    [System.Serializable]
    public class GunpowderFacilityLevels
    {
        public ulong aa;
    }
    [System.Serializable]
    public class EtherFacilityLevels
    {
        public ulong aa;
    }
    [System.Serializable]
    public class ReaMetalFacilityLevels
    {
        public ulong aa;
    }

    [System.Serializable]
    public class ForceFacilityLevels
    {
        public ulong aa;
    }

    /// <summary>
    /// イベント進行度を管理する条件
    /// </summary>
    [System.Serializable]
    public class UserEventProgress
    {
        /// <summary>
        /// イベントのID
        /// イベントの進行度は、ID＋Seqで保存する
        /// 順番に発生しない場合はSeqは１
        /// </summary>
        public int EventID;//
        public int Seq;//
        public int EventType;//
    }


}
