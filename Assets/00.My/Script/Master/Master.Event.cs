﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using UnityEngine;


namespace Master.Event
{

    public enum ClickEventType
    {
        /// <summary>
        /// 総クリック数
        /// </summary>
        TotalClick,
        /// <summary>
        /// 総プレイ時間（オフライン含む）
        /// </summary>
        TotalTime,
        /// <summary>
        /// 総プレイ時間（実行時間のみ）
        /// </summary>
        TotalPlayTime,

        /// <summary>
        /// 総プレイ資源数
        /// </summary>
        TotalResources,

        /// <summary>
        /// この周回のプレイ時間
        /// </summary>
        PestagePlayTime,
        /// <summary>
        /// この周回のクリック回数
        /// </summary>
        PrestageTotalClick,
        /// <summary>
        /// この周回のトータル資源獲得数
        /// </summary>
        PrestageTotalResources,
    }

    public enum OccurType
    {
        JustTime,       //その場で発生
        EventButton,         //実行ボタン出現
        BattleButton,


    }

    /// <summary>
    /// イベント進行度を管理する条件
    /// </summary>
    [System.Serializable]
    public class EventProgress
    {
        /// <summary>
        /// イベントのID
        /// イベントの進行度は、ID＋Seqで保存する
        /// 順番に発生しない場合はSeqは１
        /// </summary>
        public int EventID;//
        public int Seq;//
        public int EventType;//
        public int Value;//
        public string FunctionName;//
        public int OccurType;
    }


    /// <summary>
    /// イベントの会話テキスト
    /// テキストのみ保存する
    /// イベントのアクションは
    /// </summary>
    [System.Serializable]
    public class EventSpeakTextData
    {
        public int EventID;
        public int Seq;
        public int Page;
        public string MainText;
    }



}