﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public static class CV
{
    public static int ClickFrontStateTypeCnt { private set; get; } = Enum.GetValues(typeof(ClickFrontStateType)).Length;

    public static WaitForEndOfFrame WaitForEndOfFrame { private set; get; } = new WaitForEndOfFrame();

}

