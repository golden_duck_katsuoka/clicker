﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Utf8Json;
using System;
using ClickerData;
using Master.Event;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace model {

    public class ScriptableModel : ScriptableObject {

        public UserData userData;

        /*
        public List<ScriptableData> scriptableMasters;

        private void Awake() {
            if (scriptableMasters == null) {
                scriptableMasters = new List<ScriptableData>();
            }
        }

        */



        
#if UNITY_EDITOR

        public void SaveAssets()
        {
            EditorUtility.SetDirty(this);
            AssetDatabase.SaveAssets();
        }
        
        public static List<T> JsonReader<T>() {

            var fullPath = FilePath.GetFilePath<T>();

            List<T> jsonData = null;
            try {
                using (var file = new StreamReader(fullPath)) {
                    var json = file.ReadToEnd();
                    Debug.Log(json);

                    if (!string.IsNullOrEmpty(json)) {
                        jsonData = JsonSerializer.Deserialize<List<T>>(json);
                    }
                }
            }
            catch (System.Exception e) {
                Debug.LogError(e.ToString());
                throw;
            }

            return jsonData;
        }

        public void ReadTableData() {
            //scriptableMasters = JsonReader<ScriptableData>("user.txt");
        }


        /// <summary>
        /// 新規作成の扱いは検討中（デバグ機能なので不要かも）
        /// </summary>
        /// <returns></returns>
        public void CreateAssetFile() {
            ReadTableData();
            
            // ユニークなパスを生成する
            string path = AssetDatabase.GenerateUniqueAssetPath("Assets/" + typeof(ScriptableModel) + ".asset");
            Debug.Log("Path: " + path);

            // 該当パスにオブジェクトアセットを生成
            AssetDatabase.CreateAsset(this, path);

            // 未保存のアセットをアセットデータベースに保存
            AssetDatabase.SaveAssets();
        }
#endif

    }



    public class ScriptableMaster : ScriptableModel
    {
        


        

    }
}
