﻿using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading.Tasks;
using UnityEngine;

namespace Utility
{
    public class SaveUtility
    {
        public static string SavePath => Application.persistentDataPath + "/cache/";
        const string BackUpSuffix = "_bkup";
        public static string EncryptPassphrase { internal get; set; }

        public static string GetEncryptSavePath(string key)
        {
            var path = $"{SavePath}{EncryptUtility.Encrypt(key)}";
            Debug.Log("saved path " + path);
            return path;
        }

        /// <summary>
        /// 指定したキーのファイルを削除する
        /// </summary>
        /// <param name="key"></param>
        public static void Delete(string key)
        {
            var path = GetEncryptSavePath(key);
            if (File.Exists(path))
            {
                // あれば消す
                File.Delete(path);
            }
        }

        /// <summary>
        /// 指定したキーのファイルが存在するか確認する
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static bool Exists(string key)
        {
            var path = GetEncryptSavePath(key);
            return File.Exists(path);
        }

        /// <summary>
        /// セーブされているデータをすべて削除する
        /// </summary>
        /// <param name="excludeKeys">除外するキー</param>
        public static void DeleteAll(string[] excludeKeys = null)
        {
            var excludeFiles = excludeKeys?.Select(GetEncryptSavePath).ToArray();
            var deleteFiles = Directory.GetFiles(SavePath);
            foreach (var deleteFile in deleteFiles)
            {
                if (excludeFiles != null && excludeFiles.Contains(deleteFile))
                {
                    continue;
                }

                File.Delete(deleteFile);
            }
        }


        /// <summary>
        /// objectデータを保存する。
        /// </summary>
        /// <param name="key">保存キー</param>
        /// <param name="data">保存データ</param>
        public static void Save(string key, object data)
        {
            var path = GetEncryptSavePath(key);
            SaveWithPath(path, data);
#if UNITY_IOS
            UnityEngine.iOS.Device.SetNoBackupFlag(path);
#endif
        }

        /// <summary>
        /// objectデータを非同期で保存する
        /// </summary>
        /// <param name="key"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public static async Task SaveAsync(string key, object data)
        {
            var path = GetEncryptSavePath(key);
            await Task.Run(() => SaveWithPath(path, data));
#if UNITY_IOS
            UnityEngine.iOS.Device.SetNoBackupFlag(path);
#endif
        }

        internal static void SaveWithPath(string path, object data)
        {
            var dir = Path.GetDirectoryName(path);
            var backup = path + BackUpSuffix;
            if (dir == null)
            {
                return;
            }

            try
            {
                if (!Directory.Exists(dir))
                {
                    Directory.CreateDirectory(dir);
                }

                // バックアップ作成
                if (File.Exists(path))
                {
                    File.Copy(path, backup, true);
                }


                using (var cfs = new CryptoFileStream(path, FileMode.Create, FileAccess.Write,
                    EncryptPassphrase))
                {
                    IFormatter formatter = new BinaryFormatter();
                    formatter.Serialize(cfs, data);
                }
            }
            catch
            {
                // バックアップ復元
                if (File.Exists(backup))
                {
                    File.Copy(backup, path, true);
                }

                throw;
            }
        }

        /// <summary>
        /// byte配列データを保存する。
        /// </summary>
        /// <param name="key">保存キー</param>
        /// <param name="data">保存データ</param>
        public static void SaveBinary(string key, byte[] data)
        {
            var path = GetEncryptSavePath(key);
            SaveBinaryWithPath(path, data);
#if UNITY_IOS
            UnityEngine.iOS.Device.SetNoBackupFlag(path);
#endif
        }

        /// <summary>
        /// 非同期 byte配列形式のファイルを保存する
        /// </summary>
        /// <param name="key"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public static async Task SaveBinaryAsync(string key, byte[] data)
        {
            var path = GetEncryptSavePath(key);
            await Task.Run(() => SaveBinaryWithPath(path, data));
#if UNITY_IOS
            UnityEngine.iOS.Device.SetNoBackupFlag(path);
#endif
        }

        public static void SaveBinaryWithPath(string path, byte[] data)
        {
            var dir = Path.GetDirectoryName(path);
            if (dir == null)
            {
                return;
            }

            // ディレクトリの確認。
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }

            using (var cfs = new CryptoFileStream(path, FileMode.Create, FileAccess.Write,
                EncryptPassphrase))
            {
                cfs.Write(data, 0, data.Length);
            }
        }

        /// <summary>
        /// object形式に変換してファイル読込
        /// </summary>
        /// <param name="key">保存キー</param>
        public static object Load(string key)
        {
            var path = GetEncryptSavePath(key);
            return LoadWithPath(path);
        }

        internal static object LoadWithPath(string path)
        {
            object outObject;
            using (var cfs = new CryptoFileStream(path, FileMode.Open, FileAccess.Read,
                EncryptPassphrase))
            {
                IFormatter formatter = new BinaryFormatter();
                outObject = formatter.Deserialize(cfs);
            }

            return outObject;
        }

        /// <summary>
        /// byte配列形式でファイル読込
        /// </summary>
        /// <param name="key">保存キー</param>
        public static byte[] LoadBinary(string key)
        {
            var path = GetEncryptSavePath(key);
            return LoadBinaryWithPath(path);
        }

        /// <summary>
        /// 非同期 byte配列形式のファイル読込する
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static async Task<byte[]> LoadBinaryAsync(string key)
        {
            var path = GetEncryptSavePath(key);
            return await Task.Run(() => LoadBinaryWithPath(path));
        }

        public static async Task<byte[]> LoadBinaryWithPathAsync(string path)
        {
            return await Task.Run(() => LoadBinaryWithPath(path));
        }

        public static byte[] LoadBinaryWithPath(string path)
        {
            byte[] bytes;
            // ファイルチェック
            if (!File.Exists(path))
            {
                throw new FileNotFoundException();
            }

            using (var output = new MemoryStream())
            using (var cfs = new CryptoFileStream(path, FileMode.Open, FileAccess.Read,
                EncryptPassphrase))
            {
                var buffer = new byte[1024];
                var read = cfs.Read(buffer, 0, buffer.Length);
                while (read > 0)
                {
                    output.Write(buffer, 0, read);
                    read = cfs.Read(buffer, 0, buffer.Length);
                }

                bytes = output.ToArray();
            }

            return bytes;
        }

        public static async Task<byte[]> LoadBinaryAsync(Stream stream)
        {
            return await Task.Run(() => LoadBinary(stream));
        }

        public static byte[] LoadBinary(Stream stream)
        {
            byte[] bytes;
            // ファイルチェック
            if (stream == null)
            {
                throw new FileNotFoundException();
            }

            using (var cfs = new CryptoFileStream(stream, EncryptPassphrase))
            {
                var len = cfs.Length;
                bytes = new byte[len];
                cfs.Read(bytes, 0, (int)len);
            }

            return bytes;
        }
    }
}
