﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class FilePath
{
    private static string MasterFolder = "Master";


    public static string GetFilePath<T>()
    {
        var typeName = typeof(T).Name;

        switch (typeName)
        {
            case "EventProgress":
                return Path.Combine(Application.streamingAssetsPath, MasterFolder, "d_EventProgress.txt");

            case "EventSpeakTextData":
                return Path.Combine(Application.streamingAssetsPath, MasterFolder, "d_EventSpeakTextData.txt");
                
            case "FacilityName":
                return Path.Combine(Application.streamingAssetsPath, MasterFolder, "m_FacilityName.txt");
            case "FacilityBase":
                return Path.Combine(Application.streamingAssetsPath, MasterFolder, "m_FacilityBase.txt");
            case "FacilityNecessaryTable":
                return Path.Combine(Application.streamingAssetsPath, MasterFolder, "m_FacilityNecessaryTable.txt");

        }

        return "";

    }







}
