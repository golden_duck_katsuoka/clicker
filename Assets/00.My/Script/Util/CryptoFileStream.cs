﻿//
// 暗号化/復号化を行いながらファイル入出力をおこなうSteramクラス
//

using System;
using System.Diagnostics.SymbolStore;
using System.IO;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace Utility
{
    public sealed class CryptoFileStream : Stream
    {
        private CryptoStream _cryptoStream; //暗号化/復号化を行うstream

        private const int SaltLength = 16;
        private const int IvLength = 16;
        private const int BlockSize = IvLength * 8;
        private const int KeySize = SaltLength * 8;
        public const int HeaderSize = SaltLength + IvLength;
        const string DummyString = "AES encrypt.";

        static void SetupDefaultAesManaged(RijndaelManaged aes)
        {
            aes.BlockSize = BlockSize;
            aes.KeySize = KeySize;
            aes.Mode = CipherMode.CBC;
            aes.Padding = PaddingMode.PKCS7;
        }

        static ICryptoTransform CreateDefaultAesEncryptor(byte[] header, string password)
        {
            using (var aes = new RijndaelManaged())
            {
                SetupDefaultAesManaged(aes);

                var deriveBytes = new Rfc2898DeriveBytes(password, SaltLength);
                Array.Copy(deriveBytes.Salt, 0, header, 0, deriveBytes.Salt.Length);

                aes.Key = deriveBytes.GetBytes(SaltLength);
                aes.GenerateIV();

                Array.Copy(aes.IV, 0, header, aes.Key.Length, aes.IV.Length);

                return aes.CreateEncryptor(aes.Key, aes.IV);
            }
        }

        static ICryptoTransform CreateDefaultAesDecryptor(byte[] header, string password)
        {
            using (var aes = new RijndaelManaged())
            {
                SetupDefaultAesManaged(aes);

                aes.IV = new byte[IvLength];

                var salt = new byte[SaltLength];
                Array.Copy(header, 0, salt, 0, salt.Length);
                var deriveBytes = new Rfc2898DeriveBytes(password, salt);
                aes.Key = deriveBytes.GetBytes(SaltLength);

                Array.Copy(header, salt.Length, aes.IV, 0, aes.IV.Length);

                return aes.CreateDecryptor(aes.Key, aes.IV);
            }
        }

        private long _length;

        // construct
        public CryptoFileStream(string path, FileMode mode, FileAccess access, string password = null) : this(
            new FileStream(path, mode, access), password)
        {
        }

        public CryptoFileStream(string path, FileMode mode, FileAccess access, FileShare share, int bufferSize,
            bool useAsync, string password = null) : this(
            new FileStream(path, mode, access, share, bufferSize, useAsync),
            password)
        {
        }

        public CryptoFileStream(Stream fileStream, string password, CryptoStreamMode mode)
            : this(fileStream, null, password, mode)
        {
        }

        public CryptoFileStream(Stream fileStream, string password)
            : this(
                fileStream, null, password,
                fileStream.CanRead ? CryptoStreamMode.Read : CryptoStreamMode.Write)
        {
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="fileStream">ファイル入出力ストリーム</param>
        /// <param name="header">暗号化ファイルか判定したいために先にheaderSize byteだけ読み込んだ状態から始めたい場合に渡す配列</param>
        /// <param name="password">パスフレーズ</param>
        /// <param name="mode">暗号化モード</param>
        public CryptoFileStream(Stream fileStream, byte[] header, string password, CryptoStreamMode mode)
        {
            if (string.IsNullOrEmpty(password)) password = DummyString;
            ICryptoTransform cryptoTransform; //暗号化に使用する暗号化方式とそのパラメータ
            if (mode == CryptoStreamMode.Write)
            {
                header = new byte[HeaderSize];
                cryptoTransform = CreateDefaultAesEncryptor(header, password);
                //暗号キーの記録
                fileStream.Write(header, 0, header.Length);
                _length = header.Length;
            }
            else
            {
                if (header == null)
                {
                    header = new byte[HeaderSize];
                    //復号キーの読み込み
                    fileStream.Read(header, 0, header.Length);
                }

                cryptoTransform = CreateDefaultAesDecryptor(header, password);
                _length = fileStream.Length - header.Length;
            }

            _cryptoStream = new CryptoStream(fileStream, cryptoTransform, mode);
            //fileStream.Seek(header.Length, SeekOrigin.Begin);
            var dummy = new byte[16];
            if (mode == CryptoStreamMode.Write)
            {
                Write(dummy, 0, dummy.Length);
            }
            else
            {
                Read(dummy, 0, dummy.Length);
            }
        }

        public override bool CanRead => _cryptoStream.CanRead;

        public override bool CanSeek => _cryptoStream.CanSeek;

        public override bool CanWrite => _cryptoStream.CanWrite;

        public override long Length => _length;

        public override long Position
        {
            get { return _cryptoStream.Position; }
            set { _cryptoStream.Position = value; }
        }

        public override void Flush()
        {
            _cryptoStream.Flush();
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            return _cryptoStream.Read(buffer, offset, count);
        }
        public new async Task<int> ReadAsync(byte[] buffer, int offset, int count)
        {
            return await _cryptoStream.ReadAsync(buffer, offset, count);
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            throw new NotImplementedException("CryptoFileStream is not supported Seek method.");
        }

        public override void SetLength(long value)
        {
            throw new NotImplementedException("CryptoFileStream is not supported SetLength method.");
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            _cryptoStream.Write(buffer, offset, count);
            _length += count;
        }

        protected override void Dispose(bool disposing)
        {
            _cryptoStream.Dispose();
        }
    }
}
