﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Utility
{
    /// <summary>
    /// 暗号化ユーティリティー
    /// </summary>
    public class EncryptUtility
    {
        // 128bit(16byte)のIV（初期ベクタ）とKey（暗号キー）
        private const string AesIv = @"HdCzy79A5ZufHy8P";
        private const string AesKey = @"xXf8d7Ajxh7vvtVx";
        private const int SaltLength = 16;
        private const int IvLength = 16;
        private const int BlockSize = IvLength * 8;
        private const int KeySize = SaltLength * 8;

        /// <summary>
        /// 文字列をAESで暗号化
        /// </summary>
        public static string Encrypt(string text)
        {
            byte[] dest;

            var src = Encoding.Unicode.GetBytes(text);

            using (var aes = new AesCryptoServiceProvider
            {
                BlockSize = BlockSize,
                KeySize = KeySize,
                IV = Encoding.UTF8.GetBytes(AesIv),
                Key = Encoding.UTF8.GetBytes(AesKey),
                Mode = CipherMode.CBC,
                Padding = PaddingMode.PKCS7
            })
            using (var encrypt = aes.CreateEncryptor())
            {
                // 暗号化する
                dest = encrypt.TransformFinalBlock(src, 0, src.Length);
            }

            // バイト型配列からBase64形式の文字列に変換
            return Uri.EscapeDataString(Convert.ToBase64String(dest));
        }

        /// <summary>
        /// 文字列をAESで復号化
        /// </summary>
        public static string Decrypt(string text)
        {
            byte[] dest;

            var src = Convert.FromBase64String(Uri.UnescapeDataString(text));

            using (var aes = new AesCryptoServiceProvider
            {
                BlockSize = 128,
                KeySize = 128,
                IV = Encoding.UTF8.GetBytes(AesIv),
                Key = Encoding.UTF8.GetBytes(AesKey),
                Mode = CipherMode.CBC,
                Padding = PaddingMode.PKCS7
            })
            using (var decrypt = aes.CreateDecryptor())
            {
                // 複号化する
                dest = decrypt.TransformFinalBlock(src, 0, src.Length);
            }

            return Encoding.Unicode.GetString(dest);
        }

        public static string CalcSha256(Stream stream)
        {
            using (var sha = new SHA256Managed())
            {
                var hash = BitConverter.ToString(
                        sha.ComputeHash(stream)
                    )
                    .Replace("-", "")
                    .ToLower();
                stream.Position = 0;
                return hash;
            }
        }
        public static string CalcSha256(string text)
        {
            using (var ms = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(text)))
            {
                return CalcSha256(ms);
            }
        }
        public static string CalcSha256(byte[] bytes)
        {
            using (var ms = new MemoryStream(bytes))
            {
                return CalcSha256(ms);
            }
        }
    }
}
