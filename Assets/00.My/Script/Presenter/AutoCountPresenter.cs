﻿using Master.Facility;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Threading.Tasks;
using UniRx;
using UniRx.Triggers;
using UnityEngine;


public class AutoCountPresenter : MonoBehaviour
{
    [SerializeField] Model model;
    [SerializeField] View view;


    public ReactiveProperty<AutoCountState> MainState;

    private Dictionary<int, IDisposable> mainSubjectDisposers = new Dictionary<int, IDisposable>();

    private void Start()
    {

        model.Force.Subscribe(value => {
            view.ForceText = value.ToString();
        });
        model.One.Subscribe(value => {
            view.OneText = value.ToString();
        });
        model.Earth.Subscribe(value => {
            view.EarthText = value.ToString();
        });
        model.Metal.Subscribe(value => {
            view.MetalText = value.ToString();
        });
        model.Fuel.Subscribe(value => {
            view.FuelText = value.ToString();
        });
        model.Gunpowder.Subscribe(value => {
            view.GunpowderText = value.ToString();
        });
        model.Ether.Subscribe(value => {
            view.EtherText = value.ToString();
        });
        model.ReaMetal.Subscribe(value => {
            view.ReaMetalText = value.ToString();
        });

        InitObservers();
    }
    
    
    private void InitObservers()
    {
        // 初期化
        foreach (var dic in mainSubjectDisposers)
        {
            if (dic.Value is IDisposable disposable)
            {
                disposable.Dispose();
            }
        }

        model.facilityBases.ForEach( x => {
            mainSubjectDisposers.Add(x.FacilityID, null);
        });
        
        foreach(var master in model.facilityBases)
        {
            var key = mainSubjectDisposers[master.FacilityID];
            var selectMaster = model.facilityBases.Where(x => x.FacilityID == master.FacilityID);

            var resourcesType = (ResourcesType)model.facilityNames.Where(x => x.FacilityID == master.FacilityID).First().ResourcesType;
            
            // Reactive propaty set
            var resorceRactive = GetResourceReactiveValue(resourcesType);

            var userLevel = FacilityIDtoUserLevel(master);

            // これいらないっぽいテスト用
            if (userLevel > 0)
            {
                SetObserver(resorceRactive, master, userLevel);
            }
        }
    }

    private ReactiveProperty<BigInteger> GetResourceReactiveValue(ResourcesType resourcesType)
    {

        switch (resourcesType)
        {
            case ResourcesType.Force:
                return model.Force;
            case ResourcesType.One:
                return model.One;
            case ResourcesType.Metal:
                return model.Metal;
            case ResourcesType.Gunpowder:
                return model.Gunpowder;
            case ResourcesType.Fuel:
                return model.Fuel;
            case ResourcesType.ReaMetal:
                return model.ReaMetal;
            case ResourcesType.Ether:
                return model.Ether;
        }
        return model.One;
    }

    private ulong FacilityIDtoUserLevel(FacilityBase facilityBase)
    {
        var facilityName = model.facilityNames.Where(x => x.FacilityID == facilityBase.FacilityID).First();


        switch ((ResourcesType)facilityName.ResourcesType)
        {
            case ResourcesType.One:

                /*
                Net
                LargeNet
                Vacuum
                LittleDrone
                Drone
                */
                switch ((OneFacilityType)facilityName.Rank)
                {
                    case OneFacilityType.Net:
                        return model.UserData.Facilitys.One.Net;
                    case OneFacilityType.LargeNet:
                        return model.UserData.Facilitys.One.LargeNet;
                    case OneFacilityType.Vacuum:
                        return model.UserData.Facilitys.One.Vacuum;
                    case OneFacilityType.LittleDrone:
                        return model.UserData.Facilitys.One.LittleDrone;
                    case OneFacilityType.Drone:
                        return model.UserData.Facilitys.One.Drone;
                }
                break;
        }
        return 0;
    }

    private void SetObserver(ReactiveProperty<BigInteger> reactiveProperty, FacilityBase master, ulong userLevel)
    {
        mainSubjectDisposers[master.FacilityID] = Observable
        .Interval(TimeSpan.FromMilliseconds(master.IntervalSeconds * 1000))
        .Subscribe(_ => {
            Debug.Log("FacilityID : " + master.FacilityID);
            reactiveProperty.Value += Mathf.FloorToInt((master.BaseValue + master.LevelAddition) * (master.LevelMagnitude * userLevel));
        });


    }
    
}
