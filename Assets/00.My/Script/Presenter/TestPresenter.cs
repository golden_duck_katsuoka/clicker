﻿using model;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestPresenter : MonoBehaviour
{

    [SerializeField] Model _model;
    [SerializeField] View _view;

    [SerializeField] EventPresenter _eventP;
    [SerializeField] AutoCountPresenter _countP;


    public int EventID;
    public int Seq;

    public async void TestPlaySpeak()
    {
        await _eventP.StartSpeak(EventID, Seq);
    }

    public void TestSaveUserData()
    {
        _model.SaveUserData();
    }

    public void TestLoadUserData()
    {
        _model.LoadUserData();
    }
    
}
