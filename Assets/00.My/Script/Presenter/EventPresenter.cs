﻿using Master.Event;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UniRx;
using UniRx.Async;
using UniRx.Triggers;
using UnityEngine;

public class EventPresenter : MonoBehaviour
{
    [SerializeField] Model _model;
    [SerializeField] View _view;

    [SerializeField] EventPresenterDetails eventPresenterDetails;

    private float _messageSpeed = 100f;


    private async void Start()
    {
        await ResourceObserver();
    }

    public async Task ResourceObserver()
    {
        
        var CheckList = GetCheckList();
        
        while (true)
        {
            foreach (var row in CheckList)
            {

                if (CheckResource(row)) {
                    Debug.Log(row.FunctionName);
                };

                await CV.WaitForEndOfFrame;
            }

            await CV.WaitForEndOfFrame;
        }
    }
    

    private bool CheckResource(EventProgress eventProgress)
    {
        switch ( (ClickEventType)eventProgress.EventType)
        {
            case ClickEventType.TotalClick:
                break;
            case ClickEventType.TotalPlayTime:
                break;
            case ClickEventType.TotalResources:
                break;
            case ClickEventType.PrestageTotalClick:
                break;
            case ClickEventType.PrestageTotalResources:
                break;
            case ClickEventType.PestagePlayTime:
                break;
        }

        return false;
    }
    
    /// <summary>
    /// チェックリストを作成してそれをループさせる負荷軽減のため
    /// </summary>
    /// <returns></returns>
    private List<EventProgress> GetCheckList()
    {
        var list = _model.eventProgressMaster;
        var userProgressList = _model.UserData.UserEventProgress;
        var eventIDList = list.Select(x => x.EventID).Distinct();

        List<EventProgress> returnList = new List<EventProgress>();

        foreach (var eventID in eventIDList)
        {
            var userProgress = userProgressList
                .Where(x => x.EventID == eventID);

            if (userProgress.Count() == 0)
            {
                returnList.Add( list.Where(x => x.EventID == eventID).First() );
                continue;
            }
            returnList.Add(list.Where(x => x.EventID == eventID && x.Seq == userProgress.First().Seq).First());
        }

        return returnList;
    }






    public async Task StartSpeak(int EventID, int Seq)
    {
        Debug.Log("StartSpeak : EventID : " + EventID + " Seq : " + Seq);

        await CV.WaitForEndOfFrame;

        _view.EnableSpeekText = true;

        var allText = _model.eventSpeakTextDatas
            .Where(x => x.EventID == EventID && x.Seq == Seq)
            .OrderBy( x => x.Page)
            .ToList();

        //Debug.Log(allText.Count() );

        if (allText.Count() == 0)
        {
            Debug.Log("StartSpeak : Event is Not Found");
            return;
        }
        
        foreach (var x in allText)
        {
            await PutTexter(x.MainText);
        }

        _view.EnableSpeekText = false;
    }
    
    /// <summary>
    /// テキスト制御
    /// </summary>
    /// <param name="text"></param>
    /// <returns></returns>
    public async Task PutTexter(string text)
    {
        _view.SpeakText = "";

        int index = 0;
        bool loopFlag = true;

        // 一文字づつ書き込む
        var obs = Observable
            .Interval(System.TimeSpan.FromMilliseconds(_messageSpeed))
            .Take(text.Length)
            .TakeUntil(_view.SpeakTapObdetver)        // tap イベントが来たら OnComplete
            .DoOnCompleted(() => {
                _view.SpeakText = text;
                loopFlag = false;
            })
            .Subscribe(_ => {
                _view.SpeakText += text[index];
                index++;
            })
            .AddTo(this);

        while (loopFlag) { await CV.WaitForEndOfFrame; }

        // タップ待ち

        var isLoop = true;
        var disposable = _view.SpeakTapObdetver.Subscribe(_=> {
            isLoop = false;
        });

        await new WaitWhile( () => isLoop);

        disposable.Dispose();
    }


}
