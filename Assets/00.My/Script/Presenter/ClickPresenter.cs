﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using System.Numerics;
using System;
using System.Threading.Tasks;
using UniRx.Async;
using UnityEngine.EventSystems;

/// <summary>
/// 自動で増えるステート
/// </summary>
public enum AutoCountState
{
    init,
    NormalCount,
    CountStop,
}

public class ClickPresenter : MonoBehaviour
{
    [SerializeField] Model _model;
    [SerializeField] View _view;

    public ReactiveProperty<ClickFrontStateType> ClickState;

    /// <summary>
    /// 1クリック度との倍率
    /// </summary>
    private float ClickValue = 1f;
    private float ClickMagnitude = 1f;

    private void Start()
    {
        _view.OnClick_MainClickEvent += OneClick;
        _view.OnClick_FooterButtonEvent += OnClickFooterButton;

        _view.OnDragEvent += OnMouseDrag;
    }


    private void OneClick()
    {
        _model.One.Value += Mathf.FloorToInt(ClickValue * ClickMagnitude);
        Debug.Log(_model.One.Value);
    }


    public void OnClickFooterButton(ClickFrontStateType clickFrontStateType)
    {
        Debug.Log(clickFrontStateType);
    }

    public void OnMouseDrag(BaseEventData data)
    {
        //Debug.Log(data.currentInputModule.input.mousePosition);
        _model.One.Value        -= 1;
        _model.Earth.Value      += 1;
    }




}
