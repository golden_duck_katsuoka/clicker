﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using model;
using ClickerData;
using UniRx;
using System.Numerics;
using Utf8Json;
using System.IO;
using Master.Event;
using Master.Facility;

public class Model : MonoBehaviour
{
    //[SerializeField] public ScriptableModel _data;

    public UserData UserData { private set; get; } = new UserData();

    public List<EventSpeakTextData> eventSpeakTextDatas { private set; get; }
    public List<EventProgress> eventProgressMaster { private set; get; }

    public List<FacilityName>               facilityNames { private set; get; }
    public List<FacilityBase>               facilityBases { private set; get; }
    public List<FacilityNecessaryTable>     facilityNecessaryTable { private set; get; }

    public ReactiveProperty<BigInteger> Force       = new ReactiveProperty<BigInteger>();
    public ReactiveProperty<BigInteger> One         = new ReactiveProperty<BigInteger>();
    public ReactiveProperty<BigInteger> Earth       = new ReactiveProperty<BigInteger>();
    public ReactiveProperty<BigInteger> Metal       = new ReactiveProperty<BigInteger>();
    public ReactiveProperty<BigInteger> Fuel        = new ReactiveProperty<BigInteger>();
    public ReactiveProperty<BigInteger> Gunpowder   = new ReactiveProperty<BigInteger>();
    public ReactiveProperty<BigInteger> Ether       = new ReactiveProperty<BigInteger>();
    public ReactiveProperty<BigInteger> ReaMetal    = new ReactiveProperty<BigInteger>();


    private void Awake()
    {
        Application.targetFrameRate = 30;

        //UserData = _data.userData;

        InitData();

    }

    private void InitData()
    {
        Force.Value = UserData.Force;
        One.Value = UserData.One;
        Metal.Value = UserData.Metal;
        Fuel.Value = UserData.Fuel;
        Gunpowder.Value = UserData.Gunpowder;
        Ether.Value = UserData.Ether;
        ReaMetal.Value = UserData.ReaMetal;

        eventProgressMaster = ScriptableModel.JsonReader<EventProgress>();
        eventSpeakTextDatas = ScriptableModel.JsonReader<EventSpeakTextData>();

        facilityNames = ScriptableModel.JsonReader<FacilityName>();
        facilityBases = ScriptableModel.JsonReader<FacilityBase>();
        facilityNecessaryTable = ScriptableModel.JsonReader<FacilityNecessaryTable>();
    }


    public async void SaveUserData()
    {
        UserData.Force = Force.Value;
        UserData.One = One.Value;
        UserData.Metal = Metal.Value;
        UserData.Fuel = Fuel.Value;
        UserData.Gunpowder = Gunpowder.Value;
        UserData.Ether = Ether.Value;
        UserData.ReaMetal = ReaMetal.Value;

        await Utility.SaveUtility.SaveAsync("userData", UserData);
        
    }

    public void LoadUserData()
    {
        UserData = (UserData)Utility.SaveUtility.Load("userData");

        InitData();
    }


}
