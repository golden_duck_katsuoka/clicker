﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UniRx;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class View : MonoBehaviour
{
    [Header("資源数Text")]
    [SerializeField] TMPro.TextMeshProUGUI Force;
    public string ForceText { set { Force.text = value; } get { return Force.text; } }

    [SerializeField] TMPro.TextMeshProUGUI One;
    public string OneText { set { One.text = value; } get { return One.text; } }

    [SerializeField] TMPro.TextMeshProUGUI Earth;
    public string EarthText { set { Earth.text = value; } get { return Earth.text; } }

    [SerializeField] TMPro.TextMeshProUGUI Metal;
    public string MetalText { set { Metal.text = value; } get { return Metal.text; } }

    [SerializeField] TMPro.TextMeshProUGUI Fuel;
    public string FuelText { set { Fuel.text = value; } get { return Fuel.text; } }

    [SerializeField] TMPro.TextMeshProUGUI Gunpowder;
    public string GunpowderText { set { Gunpowder.text = value; } get { return Gunpowder.text; } }

    [SerializeField] TMPro.TextMeshProUGUI Ether;
    public string EtherText { set { Ether.text = value; } get { return Ether.text; } }

    [SerializeField] TMPro.TextMeshProUGUI ReaMetal;
    public string ReaMetalText { set { ReaMetal.text = value; } get { return ReaMetal.text; } }

    

    [Header("Shop UI")]
    [SerializeField] GameObject ShopUI;

    [Header("Speak Text")]
    [SerializeField] GameObject SpeakTextParent;
    public bool EnableSpeekText { set { SpeakTextParent.SetActive(value); } get { return SpeakTextParent.activeInHierarchy; } }

    [SerializeField] Text _speakText;
    public string SpeakText { set { _speakText.text = value; } get { return _speakText.text; } }

    public Subject<int> SpeakTapObdetver { private set; get; } = new Subject<int>();


    [Header("Footer")]
    [SerializeField] GameObject Footer;
    [SerializeField] GameObject[] FooterGroup;

    [Header("制限クリックモード")]
    [SerializeField] Button[] FooterIcons;

    

    public System.Action OnClick_MainClickEvent;
    
    public System.Action<ClickFrontStateType> OnClick_FooterButtonEvent;

    public System.Action<BaseEventData> OnDragEvent;

    private void Start()
    {
        for (int i = 0; i < CV.ClickFrontStateTypeCnt; i++)
        {
            var index = i;
            FooterIcons[index].onClick.AddListener( () => { OnClick_FooterIcon(index); } );
        }
        
    }

    public void OnClick_SpeakTextBox()
    {
        SpeakTapObdetver.OnNext(0);
    }


    public void OnButton_ShowShopUI()
    {
        ShopUI.SetActive( !ShopUI.activeSelf);
    }

    public void OnClick_Main()
    {
        OnClick_MainClickEvent?.Invoke();
    }

    public void OnClick_FooterIcon(int footerIndex)
    {
        OnClick_FooterButtonEvent((ClickFrontStateType)footerIndex);
    }

    public void OnClick_SideButtonResource()
    {
        Footer.SetActive(!Footer.activeInHierarchy);
        FooterGroup[0].SetActive(true);
        FooterGroup[1].SetActive(false);
    }
    
    public void OnClick_SideButtonComposition()
    {
        Footer.SetActive(!Footer.activeInHierarchy);
        FooterGroup[0].SetActive(false);
        FooterGroup[1].SetActive(true);
    }

    public void OnDrag_Main(BaseEventData data)
    {
        OnDragEvent?.Invoke(data);
    }

    public void OnDrag_Main2(PointerEventData data)
    {
        
    }

}
