﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(TestPresenter))]
public class TestPresenterInspecter : Editor
{

    /// <summary>
    /// InspectorのGUIを更新
    /// </summary>
    public override void OnInspectorGUI()
    {
        //元のInspector部分を表示
        base.OnInspectorGUI();

        //ボタンを表示
        if (GUILayout.Button("Save User Data"))
        {
            //targetを変換して対象を取得
            var exampleScript = target as TestPresenter;

            exampleScript.TestSaveUserData();
        }

        //ボタンを表示
        if (GUILayout.Button("Load User Data"))
        {
            //targetを変換して対象を取得
            var exampleScript = target as TestPresenter;

            exampleScript.TestLoadUserData();
        }


        //ボタンを表示
        if (GUILayout.Button("Start SpeakText"))
        {
            //targetを変換して対象を取得
            var exampleScript = target as TestPresenter;

            exampleScript.TestPlaySpeak();
        }


        //ボタンを表示
        if (GUILayout.Button("SaveAssets"))
        {

            //targetを変換して対象を取得
            var exampleScript = target as TestPresenter;

            //var userdata = exampleScript.GetMasterData();

            //userdata.SaveAssets();
        }


    }

}